#version 330 core

in vec2 frag_tex_coord;
out vec4 color_output;

uniform vec4 u_color;
uniform sampler2D u_Texture;

void main()
{
	vec4 texColor = texture(u_Texture, frag_tex_coord);
	color_output = texColor;
}