#include "headers/test_texture2D.h"
#include "../headers/renderer.h"
#include "../vendor/imgui/imgui.h"
#include "../headers/vertex_buffer_layout.h"

namespace test {

	TestTexture2D::TestTexture2D() 
		: translationA(200, 200, 0), translationB(400, 200, 0),
		projection_matrix(glm::ortho(0.0f, 960.0f, 0.0f, 540.0f, -1.0f, 1.0f)),
		view_matrix(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))) {

		float vertices[] = {
			// positions	      //texcoords
			-50.0f, -50.0f, 0.0f, 0.0f, 0.0f,
			 50.0f, -50.0f, 0.0f, 1.0f, 0.0f,
			 50.0f,  50.0f, 0.0f, 1.0f, 1.0f,
			-50.0f,  50.0f, 0.0f, 0.0f, 1.0f,
		};

		unsigned int indices[] = {
			0, 1, 2,
			2, 3, 0
		};

		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		vertex_array = std::make_unique<VertexArray>();
		vertex_buffer = std::make_unique<VertexBuffer>(vertices, sizeof(vertices));
		VertexBufferLayout layout;
		layout.push<float>(3);
		layout.push<float>(2);

		vertex_array->add_buffer(*vertex_buffer, layout);
		index_buffer = std::make_unique<IndexBuffer>(indices, sizeof(indices));
		shader = std::make_unique<Shader>("C:/sh1tz/apesticks/OpenGL/OpenGL/OpenGL/res/shaders/vertex_shader.glsl", "C:/sh1tz/apesticks/OpenGL/OpenGL/OpenGL/res/shaders/fragment_shader.glsl");
		shader->bind();
		shader->set_uniform_4f("u_color", 0.9f, 0.2f, 0.1f, 1.0f);
		texture = std::make_unique<Texture>("res/textures/evil_monkey.png");
		shader->set_uniform_1i("u_Texture", 0);
	}

	TestTexture2D::~TestTexture2D() {
	}

	void TestTexture2D::on_update(float delta_time) {
	}

	void TestTexture2D::on_render() {
		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
		GLCall(glClear(GL_COLOR_BUFFER_BIT));

		Renderer renderer;
		texture->bind();
		{
			glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f), translationA);
			glm::mat4 mvp = projection_matrix * view_matrix * model_matrix;
			shader->bind();
			shader->set_uniform_mat4f("u_MVP", mvp);
			renderer.draw(*vertex_array, *index_buffer, *shader);
		}
		{
			glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f), translationB);
			glm::mat4 mvp = projection_matrix * view_matrix * model_matrix;
			shader->bind();
			shader->set_uniform_mat4f("u_MVP", mvp);
			renderer.draw(*vertex_array, *index_buffer, *shader);
		}
	}

	void TestTexture2D::on_imgui_render() {
		ImGui::SliderFloat2("Translation A", &translationA.x, 0.0f, 960.0f);
		ImGui::SliderFloat2("Translation B", &translationB.x, 0.0f, 960.0f);
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}
}
