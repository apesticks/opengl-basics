#include "headers/test_texture2D_RTS.h"
#include "../headers/renderer.h"
#include "../vendor/imgui/imgui.h"
#include "../headers/vertex_buffer_layout.h"
#include "glfw3.h"

namespace test {

	TestTexture2DRTS::TestTexture2DRTS() 
		: translationA(200, 200, 0), translationB(400, 200, 0),
		projection_matrix(glm::ortho(0.0f, 960.0f, 0.0f, 540.0f, -1.0f, 1.0f)),
		view_matrix(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),
		rotation(glm::rotate(rotation, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f))),
		rads(0.0f), scale(1.f){

		float vertices[] = {
			// positions	      //texcoords
			-50.0f, -50.0f, 0.0f, 0.0f, 0.0f,
			 50.0f, -50.0f, 0.0f, 1.0f, 0.0f,
			 50.0f,  50.0f, 0.0f, 1.0f, 1.0f,
			-50.0f,  50.0f, 0.0f, 0.0f, 1.0f,
		};

		unsigned int indices[] = {
			0, 1, 2,
			2, 3, 0
		};

		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		vertex_array = std::make_unique<VertexArray>();
		vertex_buffer = std::make_unique<VertexBuffer>(vertices, sizeof(vertices));
		VertexBufferLayout layout;
		layout.push<float>(3);
		layout.push<float>(2);

		vertex_array->add_buffer(*vertex_buffer, layout);
		index_buffer = std::make_unique<IndexBuffer>(indices, sizeof(indices));
		shader = std::make_unique<Shader>("C:/sh1tz/apesticks/OpenGL/OpenGL/OpenGL/res/shaders/vertex_shader.glsl", "C:/sh1tz/apesticks/OpenGL/OpenGL/OpenGL/res/shaders/fragment_shader.glsl");
		shader->bind();
		shader->set_uniform_4f("u_color", 0.9f, 0.2f, 0.1f, 1.0f);
		texture = std::make_unique<Texture>("res/textures/evil_monkey.png");
		shader->set_uniform_1i("u_Texture", 0);
	}

	TestTexture2DRTS::~TestTexture2DRTS() {
	}

	void TestTexture2DRTS::on_update(float delta_time) {
	}

	void TestTexture2DRTS::on_render() {
		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
		GLCall(glClear(GL_COLOR_BUFFER_BIT));

		Renderer renderer;
		texture->bind();
		{
			glm::mat4 model_matrix = glm::translate(glm::mat4(1.0f), translationA);
			model_matrix = glm::rotate(model_matrix, glm::radians(rads), rotationA);
			model_matrix = glm::scale(model_matrix, scale);

			glm::mat4 mvp = projection_matrix * view_matrix * model_matrix;
			shader->bind();
			shader->set_uniform_mat4f("u_MVP", mvp);
			renderer.draw(*vertex_array, *index_buffer, *shader);
		}
	}

	void TestTexture2DRTS::on_imgui_render() {
		ImGui::SliderFloat3("Translation A", &translationA.x, 0.0f, 960.0f);
		ImGui::SliderFloat3("Scale", &scale.x, -10.0f, 10.0f);
		ImGui::SliderFloat3("Rotation Axis", &rotationA.x, 0.0f, 1.0f);
		ImGui::SliderFloat("Rads", &rads, 0.0f, 360.0f);
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}
}
