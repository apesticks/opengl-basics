#pragma once

#include "test.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"

#include "../../headers/vertex_array.h"
#include "../../headers/vertex_buffer.h"
#include "../../headers/index_buffer.h"
#include "../../headers/shader.h"
#include "../../headers/texture.h"
#include <memory>

namespace test {
	class TestTexture2D : public Test {
	public: 
		TestTexture2D();
		~TestTexture2D();

		void on_update(float delta_time) override;
		void on_render() override;
		void on_imgui_render() override;

	private:
		std::unique_ptr<VertexArray> vertex_array;
		std::unique_ptr<VertexBuffer> vertex_buffer;
		std::unique_ptr<IndexBuffer> index_buffer;
		std::unique_ptr<Shader> shader;
		std::unique_ptr<Texture> texture;

		glm::mat4 projection_matrix, view_matrix;
		glm::vec3 translationA, translationB;
		glm::mat4 rotation;
	};
}
