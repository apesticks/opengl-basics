#include "headers\test.h"
#include "..\vendor\imgui\imgui.h"

namespace test {
	TestMenu::TestMenu(Test *& current_test_pointer) : current_test(current_test_pointer) {
	}

	void test::TestMenu::on_imgui_render() {
		for (auto& test : tests) {
			if (ImGui::Button(test.first.c_str())) {
				current_test = test.second();
			}
		}
	}
}
