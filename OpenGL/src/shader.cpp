#include "headers/shader.h"

#include <fstream>
#include <iostream>

#include "headers/renderer.h"

Shader::Shader(const std::string& vert_file_path, const std::string& frag_file_path)
	: vert_file_path(vert_file_path), frag_file_path(frag_file_path), renderer_id(0) {
	const std::string vertex_shader_source = read_shader(vert_file_path);
	const std::string fragment_shader_source = read_shader(frag_file_path);
	renderer_id = create_shader(vertex_shader_source, fragment_shader_source);
}

Shader::~Shader() {
	GLCall(glDeleteProgram(renderer_id))
}

void Shader::bind() const {
	GLCall(glUseProgram(renderer_id));
}

void Shader::unbind() const {
	GLCall(glUseProgram(0));
}

void Shader::set_uniform_mat4f(const std::string & name, const glm::mat4& matrix) {
	GLCall(glUniformMatrix4fv(get_uniform_location(name), 1, GL_FALSE, &matrix[0][0]));
}

void Shader::set_uniform_1i(const std::string & name, int value) {
	GLCall(glUniform1i(get_uniform_location(name), value));
}

void Shader::set_uniform_1f(const std::string & name, float value) {
	GLCall(glUniform1f(get_uniform_location(name), value));
}

void Shader::set_uniform_4f(const std::string & name, float v0, float v1, float v2, float v3) {
	GLCall(glUniform4f(get_uniform_location(name), v0, v1, v2, v3));
}

int Shader::get_uniform_location(const std::string & name) {
	if (uniform_location_cache.find(name) != uniform_location_cache.end())
		return uniform_location_cache[name];

	GLCall(int location = glGetUniformLocation(renderer_id, name.c_str()));
	if (location == -1)
		std::cout << "Warning: uniform '" << name << "' doesn't exist!" << std::endl;

	uniform_location_cache[name] = location;
	return location;
}

unsigned int Shader::validate_shader_compilation(unsigned int shader_id, unsigned int shader_type) {
	int  success;
	char info_log[512];
	GLCall(glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success));

	if (!success) {
		GLCall(glGetShaderInfoLog(shader_id, 512, NULL, info_log));
		std::cout << "Failed to cmpile " << (shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment") << std::endl;
		std::cout << info_log << std::endl;

		GLCall(glDeleteShader(shader_id));

		return 0;
	}

	return shader_id;
}

unsigned int Shader::compile_shader(unsigned int shader_type, const std::string & source) {
	unsigned int shader_id = glCreateShader(shader_type);
	const char* src = source.c_str();
	GLCall(glShaderSource(shader_id, 1, &src, nullptr));
	GLCall(glCompileShader(shader_id));

	return validate_shader_compilation(shader_id, shader_type);
}

unsigned int Shader::create_shader(const std::string & vertex_shader, const std::string & fragment_shader) {
	unsigned int program = glCreateProgram();
	unsigned int vs = compile_shader(GL_VERTEX_SHADER, vertex_shader);
	unsigned int fs = compile_shader(GL_FRAGMENT_SHADER, fragment_shader);

	GLCall(glAttachShader(program, vs));
	GLCall(glAttachShader(program, fs));
	GLCall(glLinkProgram(program));
	GLCall(glValidateProgram(program));

	GLCall(glDeleteShader(vs));
	GLCall(glDeleteShader(fs));

	return program;
}

const std::string Shader::read_shader(const std::string & shader_path) {
	std::string temp = "";
	std::string source = "";
	std::ifstream in_file;

	in_file.open(shader_path);

	if (in_file.is_open()) {
		while (std::getline(in_file, temp))
			source += temp + "\n";
	}

	in_file.close();

	return source.c_str();
}
