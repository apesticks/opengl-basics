#pragma once

#include <string>

class Texture {
private:
	unsigned int renderer_id;
	std::string file_path;
	unsigned char* local_buffer;
	int width, height, bpp;
public:
	Texture(const std::string path);
	~Texture();

	void bind(unsigned int slot = 0) const;
	void unbind() const;

	inline int get_width() const { return width; }
	inline int get_height() const { return height; }
};
