#pragma once
#include <string>
#include <unordered_map>
#include "glm.hpp"

class Shader {
private:
	std::string vert_file_path;
	std::string frag_file_path;
	unsigned int renderer_id;
	std::unordered_map<std::string, int> uniform_location_cache;

public:
	Shader(const std::string& vert_file_path, const std::string& frag_file_path);
	~Shader();

	void bind() const;
	void unbind() const;

	void set_uniform_mat4f(const std::string& name, const glm::mat4& matrix);
	void set_uniform_1i(const std::string& name, int value);
	void set_uniform_1f(const std::string& name, float value);
	void set_uniform_4f(const std::string& name, float v0, float v1, float v2, float v3);

private:
	int get_uniform_location(const std::string& name);
	unsigned int validate_shader_compilation(unsigned int shader_id, unsigned int shader_type);
	unsigned int compile_shader(unsigned int shader_type, const std::string& source);
	unsigned int create_shader(const std::string& vertex_shader, const std::string& fragment_shader);
	const std::string read_shader(const std::string& shader_path);
};