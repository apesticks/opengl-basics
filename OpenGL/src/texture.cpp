#include "headers/texture.h"
#include "headers/renderer.h"
#include "vendor/stb_image/stb_image.h"

Texture::Texture(const std::string path)
	: renderer_id(0), file_path(path), local_buffer(0), width(0), height(0), bpp(0) {

	stbi_set_flip_vertically_on_load(1);
	local_buffer = stbi_load(path.c_str(), &width, &height, &bpp, 4);

	GLCall(glGenTextures(1, &renderer_id));
	GLCall(glBindTexture(GL_TEXTURE_2D, renderer_id));

	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

	GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, local_buffer));
	unbind();

	if (local_buffer) {
		stbi_image_free(local_buffer);
	}
}

Texture::~Texture() {
	GLCall(glDeleteTextures(1, &renderer_id));
}

void Texture::bind(unsigned int slot) const {
	GLCall(glActiveTexture(GL_TEXTURE0 + slot));
	GLCall(glBindTexture(GL_TEXTURE_2D, renderer_id));
}

void Texture::unbind() const {
	GLCall(glBindTexture(GL_TEXTURE_2D, 0));
}