#include <glew.h>
#include <glfw3.h>
#include <iostream>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>

#include "headers/vertex_array.h"
#include "headers/vertex_buffer.h"
#include "headers/vertex_buffer_layout.h"
#include "headers/index_buffer.h"
#include "headers/shader.h"
#include "headers/texture.h"
#include "headers/renderer.h"

#include "vendor/imgui/imgui.h"
#include "vendor/imgui/imgui_impl_glfw_gl3.h"
#include "tests/headers/test_clear_color.h"
#include "tests/headers/test_texture2D.h"
#include "tests/headers/test_texture2D_RTS.h"

const unsigned int SCR_WIDTH = 960;
const unsigned int SCR_HEIGHT = 540;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void process_input(GLFWwindow *window);

int main() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "OpenGL Basics", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK) {
		std::cout << "Failed to initialize GLEW" << std::endl;
	}

	{
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		Renderer renderer;

		ImGui::CreateContext();
		ImGui_ImplGlfwGL3_Init(window, true);
		ImGui::StyleColorsDark();

		test::Test* current_test = nullptr;
		test::TestMenu* test_menu = new test::TestMenu(current_test);
		current_test = test_menu;

		test_menu->register_test<test::TestClearColor>("Clear Color");
		test_menu->register_test<test::TestTexture2D>("2D Texture");
		test_menu->register_test<test::TestTexture2DRTS>("2D TextureRTS");

		while (!glfwWindowShouldClose(window)) {
			process_input(window);

			renderer.clear();

			ImGui_ImplGlfwGL3_NewFrame();
			if (current_test) {
				current_test->on_update(0.0f);
				current_test->on_render();
				ImGui::Begin("Test");
				if (current_test != test_menu && ImGui::Button("<-")) {
					delete current_test;
					current_test = test_menu;
				}
				current_test->on_imgui_render();
				ImGui::End();
			}

			ImGui::Render();
			ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

			GLCall(glfwSwapBuffers(window));
			GLCall(glfwPollEvents());
		}
	}

	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();
	glfwTerminate();
	return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

void process_input(GLFWwindow *window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}