#include "headers/vertex_buffer.h"
#include "headers/renderer.h"

VertexBuffer::VertexBuffer(const void *data, unsigned int size) {
	GLCall(glGenBuffers(1, &render_id));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, render_id));
	GLCall(glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW));
}

VertexBuffer::~VertexBuffer() {
	GLCall(glDeleteBuffers(1, &render_id));
}

void VertexBuffer::bind() const {
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, render_id));
}

void VertexBuffer::unbind() const {
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
}
